angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('MusicCtrl', ['$scope', '$rootScope', 'MediaManager','audioFactory', function($scope, $rootScope, MediaManager, audioFactory) {

  $rootScope.dynamicTrack = {};
  $rootScope.index=0;

  audioFactory.$loaded().then(function () {
    $rootScope.tracks=audioFactory;
    $rootScope.dynamicTrack = $rootScope.tracks[0];
  });

  $rootScope.playPreviousTrack=function () {
    $rootScope.index--;
    $rootScope.playTrack($rootScope.index);
  };
  $rootScope.playNextTrack=function () {
    $rootScope.index++;
    $rootScope.playTrack($rootScope.index);
  };
  $rootScope.stopPlayback = function() {
    MediaManager.stop();
  };
  $rootScope.playTrack = function(index) {
    // console.log("Playing index", index);
    $rootScope.dynamicTrack = $rootScope.tracks[index];
    $rootScope.togglePlayback = !$rootScope.togglePlayback;
  };
}])

.controller('myLibraryCtrl', ['$rootScope', 'audioFactory', function ($rootScope, audioFactory) {
  var myLib=this;
  if(!$rootScope.tracks){
    audioFactory.$loaded().then(function () {
      myLib.tracksList = audioFactory;
    });
  }else {
    myLib.tracksList=$rootScope.tracks;
  }

  // audioFactory.$add(
  //   {
  //     url: 'https://dl.dropbox.com/s/31t1zon1s2lpi9e/O%20mere%20dil%20ke%20chain%20-%20Luv.mp3?dl=1',
  //     artist: 'Shubham',
  //     title: 'Chain - (Cover)',
  //     art: 'img/ionic.png'
  //   }
  // );
}]);
